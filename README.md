# LaTeX Style

Style and configuration for the LaTeX documents of the Bern RTOS project.

## Usage

Add this repository as a submodule (`https` makes it easier for GitLab CI):
```bash
git submodule add https://gitlab.com/bern-rtos/doc/tools/latex-style.git extra
```

Add information about the document to your main file:
```latex
\newcommand{\documenttitleshort}{Bern\ RTOS}
\newcommand{\documenttype}{Project I}
\newcommand{\documentauthor}{Stefan\ Lüthi}
\newcommand{\organization}{Berner Fachhochschule | Haute école spécialisée bernoise | Bern University of Applied Sciences}
\newcommand{\version}{1.0}
\newcommand{\versiondate}{10.01.2020}
```

and import the style/configuration:
```latex
\input{extra/paper_format.tex}
\input{extra/config.tex}
```

## License

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This <span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" rel="dct:type">work</span> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
